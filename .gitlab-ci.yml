#
#  Copyright (c) 2020 - 2021 Henix, henix.fr
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#


variables:
  # Here are variables visible in Gitlab UI when launching manually a pipeline
  # Init version variables
  BUNDLE_VERSION: ""
  IMAGE_VERSION: ""
  ORCHESTRATOR_VERSION: ""
  QUALITYGATE_VERSION: ""
  PYTHON_PLUGINS_VERSION: ""
  PYTHON_IMAGE_VERSION:
    value: "3.12"
    description: "Python version used in the image"
  PATH_TO_OPERATOR:
    value: "/usr/local/lib/python${PYTHON_IMAGE_VERSION}/site-packages/opentf/operator/main.py"
    description: "Path to the operator"
  # Default value
  IMAGE_PATH: "opentestfactory"
  IMAGE_NAME: "agentoperator"
  # BASE_BOM_NAME: "otf-images"
  IS_NIGHTLY: "false"
  # Variable for the version status ( is latest )
  VERSION_STATUS_FILE: "version-status"
  VERSION_STATUS_IS_LATEST_KEY: "IS_LATEST"  
  RUN_UNIT_TEST: "true"
  SOURCE_DIR: "opentf"
  TEST_DIR: "tests/python"
  # Analyzer options (CS = container scanning)
  CS_IMAGE: 
    value: "docker-group.squashtest.org/opentestfactory/agentoperator:nightly"
    description: "Set this variable to test another image than the nightly"
  CS_REGISTRY_USER: ${DOCKER_INTERNAL_REGISTRIES_USER}
  CS_REGISTRY_PASSWORD: ${DOCKER_INTERNAL_REGISTRIES_PASSWORD}

  # DIND Variables: 
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_TLS_VERIFY: 1
  DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"

  APT_REPOSITORY:
    value: "${APT_DEBIAN_BOOKWORM_REPOSITORY}"
    description: "Change the apt repository added to the image. The possible variables are: APT_DEBIAN_BOOKWORM_REPOSITORY, APT_DEBIAN_BULLSEYE_REPOSITORY, APT_UBUNTU_FOCAL_REPOSITORY or APT_UBUNTU_JAMMY_REPOSITORY"

include:
    - template: Jobs/Dependency-Scanning.latest.gitlab-ci.yml
    - template: Jobs/Secret-Detection.latest.gitlab-ci.yml
    - template: Security/Container-Scanning.gitlab-ci.yml

stages:
- init-image
- validate
- unit-test
- lint
- build-wheel
- get-destinations
- build-image
- docker:start
- test-built-image
- push-image
- test # test the image
## DIRTY: test only nightly for now
- docker:signature


# In this job ( which runs only when we are in a final release process ), we define if this final release is the latest version of the projet and if it is the latest version for the current major ( the X in vX.Y.Z)
# The results are put in a file define as an artifact of the job. Then the job "image-build" in the next stage can retrieve it and use it.
last-tags:
  stage: init-image
  image: bitnami/git
  script:
    # We retrieve all tags from repository
    - git fetch --tags
    # # We retrieve, as an ordered array, all the "final release" tags in the project.
    # `git --no-pager tag -l --sort=-v:refname  v*.*.*` : provide a list of project's tag ordered by version and matching the pattern v*
    # We need a more accurate pattern matching, hence the use of grep
    - ALL_TAG_LIST=($(git --no-pager tag -l --sort=-v:refname  v*-*.* | grep -E "v([0-9]{4})-([0-9]{2})\.([0-9]{2})$" ))
    # Note : the ALL_TAG_LIST array can't be empty as it should contains the tag that triggered the current release execution
    - if [[ "${#ALL_TAG_LIST[@]}" -eq "0" ]]; then echo "[ERROR] No tag retrieved, this shouldn't be possible. End of pipeline" && exit 1; fi
    # If the first element of the ALL_TAG_LIST array is equal to our current tag, that means we are releasing the latest version
    - |
      if [[ "${CI_COMMIT_TAG}" == "${ALL_TAG_LIST[0]}" ]]
      then
        echo "${CI_COMMIT_TAG} is the latest version"
        echo "${VERSION_STATUS_IS_LATEST_KEY}=true" > ${VERSION_STATUS_FILE}
      else
        echo "${CI_COMMIT_TAG} is not the latest version"
        echo "${VERSION_STATUS_IS_LATEST_KEY}=false" > ${VERSION_STATUS_FILE}
      fi
  artifacts:
    paths: 
      - ./${VERSION_STATUS_FILE}
    expire_in: 10 mins
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v(\d{4})-(\d{2})\.(\d{2})$/ &&  $CI_COMMIT_REF_PROTECTED == "true"'

format-check:
  stage: validate
  image: python
  before_script:
    # black install
    - pip install black
  script:
    # code format checking with black
    - black . --check --diff --color
  # No execution on "tag" event
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success

test:
  stage: unit-test
  image: python:${PYTHON_IMAGE_VERSION}
  before_script:
    # Install required component
    - pip install unittest-xml-reporting coverage -e .
  script:
    # Unit test execution and coverage analysis
    - coverage run --source=${SOURCE_DIR} -m xmlrunner discover -s ${TEST_DIR} && coverage xml
  artifacts:
    # Unit tests and coverage reports stocked as gitlab-ci artifact in order to be used by next jobs
    paths:
      - TEST-*.xml
      - coverage.xml
    # Unit tests and coverage reports provided to gitlab for usage in gitlab UI
    reports:
      junit: TEST-*.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    expire_in: 1 week
  # No execution on "tag" event
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    # Execution if TUN_UNIT_TEST is 'true' and there is file matching the pattern 'tests/python/test*.py'
    - if: $RUN_UNIT_TEST == "true"
      exists:
        - tests/python/test*.py

dockerfile_lint:
  stage: lint
  image: hadolint/hadolint:2.12.0-debian
  script:
    - hadolint Dockerfile --failure-threshold error
  tags:
    - ovh-kosmos

get-dest-and-tag:
  stage: get-destinations
  image: alpine:latest
  variables:
    DESTINATION: ""
    DOCKER_TAG: ""
    REGISTRY_URL: "${DOCKER_TEMP_INTERNAL_REGISTRY_URL}"
    REGISTRY_USER: "${DOCKER_INTERNAL_REGISTRIES_USER}"
    REGISTRY_PASSWORD : "${DOCKER_INTERNAL_REGISTRIES_PASSWORD}"
  script:
    - |
      if [[ "${BUILD_TYPE}" == "branch" ]]
      then
        echo "Is branch"
        DOCKER_TAG=${CI_COMMIT_BRANCH}
        DESTINATION="--destination=${REGISTRY_URL}/${IMAGE_PATH}/${IMAGE_NAME}:${DOCKER_TAG}-latest"
        DESTINATION="${DESTINATION} --destination=${REGISTRY_URL}/${IMAGE_PATH}/${IMAGE_NAME}:${DOCKER_TAG}-${CI_PIPELINE_ID}"
      fi
    - |
      if [[ "${BUILD_TYPE}" == "nightly" ]]
      then
        echo "Is nightly"
        DOCKER_TAG=nightly
        BUILD_DATE=$(date '+%Y_%m_%d')
        DESTINATION="${DESTINATION} --destination=${REGISTRY_URL}/${IMAGE_PATH}/${IMAGE_NAME}:nightly"
        DESTINATION="${DESTINATION} --destination=${REGISTRY_URL}/${IMAGE_PATH}/${IMAGE_NAME}:nightly-${BUILD_DATE}-${CI_PIPELINE_ID}"
        # DESTINATION="${DESTINATION} --destination=${IMAGE_PATH}/${IMAGE_NAME}:nightly" #push docker hub
      fi
    - |
      if [[ "${BUILD_TYPE}" == "rc" ]]
      then
        echo "Is RC"
        DOCKER_TAG=$(echo ${CI_COMMIT_TAG#v} | tr '[:lower:]' [:upper:])
        DESTINATION="--destination=${REGISTRY_URL}/${IMAGE_PATH}/${IMAGE_NAME}:${DOCKER_TAG}"
      fi
    - |
      if [[ "${BUILD_TYPE}" == "release" ]]
      then
        echo "Is Release"
        cat ./${VERSION_STATUS_FILE}
        source ./${VERSION_STATUS_FILE}   # set IS_LATEST as environment variable
        DOCKER_TAG=${CI_COMMIT_TAG#v}
        SHORT_TAG=$(echo "${DOCKER_TAG}" | awk '{n=split($0,a,"."); print a[1]}')
        DESTINATION="--destination=${IMAGE_PATH}/${IMAGE_NAME}:${DOCKER_TAG}"
        DESTINATION="${DESTINATION} --destination=${IMAGE_PATH}/${IMAGE_NAME}:${SHORT_TAG}"
        if [[ "${IS_LATEST}" == "true" ]]
        then
          DESTINATION="${DESTINATION} --destination=${IMAGE_PATH}/${IMAGE_NAME}:latest"
        fi
        # give all tags for signature
        touch release-tag.env
        echo "SHORT_TAG=${SHORT_TAG}" >> release-tag.env
        echo "DOCKER_TAG=${DOCKER_TAG}" >> release-tag.env
        echo "LATEST=latest" >> release-tag.env

      fi
    - echo $DESTINATION > destinations.txt
    - |
      echo '{"destinations": [' > destinations.json
      echo "$(echo $DESTINATION | sed 's/--destination=//g' | sed 's/ /", "/g' | sed 's/^/"/' | sed 's/$/"/' | tr '\n' ',')" >> destinations.json
      sed -i '$ s/,$//' destinations.json
      echo ']}' >> destinations.json
    - cat destinations.json
  artifacts:
    reports:
      dotenv:
        - release-tag.env
    paths:
      - ./destinations.txt
      - ./destinations.json
  rules:
    - if: '$IS_NIGHTLY == "true" && $CI_COMMIT_BRANCH == "main" && $CI_PIPELINE_SOURCE == "pipeline"'
      variables:
        BUILD_TYPE: "nightly"
        ENVIRONMENT: development
    - if: '$IS_NIGHTLY == "false" && $CI_COMMIT_TAG == null && ($CI_PIPELINE_SOURCE == "web" || $CI_PIPELINE_SOURCE == "pipeline")'
      variables:
        BUILD_TYPE: "branch"
        ENVIRONMENT: development
    - if: '$CI_COMMIT_TAG =~ /^v(\d{4})-(\d{2})\.(\d{2})_rc(\d+)$/ && $CI_COMMIT_REF_PROTECTED == "true"'
      variables:
        BUILD_TYPE: "rc"
        ENVIRONMENT: staging
    - if: '$CI_COMMIT_TAG =~ /^v(\d{4})-(\d{2})\.(\d{2})$/ && $CI_COMMIT_REF_PROTECTED == "true"'
      variables:
        BUILD_TYPE: "release"
        ENVIRONMENT: production




wheel-build:
  stage: build-wheel
  image: python:${PYTHON_IMAGE_VERSION}
  before_script:
    - pip install wheel setuptools
  script:
    - echo "---- Début de la création du fichier wheel ----"
    - python setup.py bdist_wheel
  rules: 
    !reference [get-dest-and-tag, rules]
  artifacts:
    paths:
      - dist/*.whl


image-build:
  stage: build-image
  image: 
    name: gcr.io/kaniko-project/executor:v1.10.0-debug
    entrypoint: [""]
  variables:
    !reference [get-dest-and-tag, variables]
  rules:
    !reference [get-dest-and-tag, rules]
  script:
    - |-
      echo -e "[global]
      extra-index-url=https://${PYPI_INTERNAL_REPO_USERNAME}:${PYPI_INTERNAL_REPO_PASSWORD}@${PYPI_INTERNAL_GROUP_REPO_URL##https://}/simple/" > pip.conf
    # Docker image creation
    - DESTINATION=$(cat destinations.txt)
    - |
        if [[ -z "${DESTINATION}" ]]
        then
          echo "No 'DESTINATION' defined" && exit 1
        else
          echo "Destination is : ${DESTINATION}"
        fi
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --registry-mirror=${DOCKER_MIRROR_INTERNAL_REGISTRY_URL}
      ${DESTINATION}
      --no-push
      --build-arg PYTHON_VERSION=${PYTHON_IMAGE_VERSION}
      --build-arg PATH_TO_OPERATOR=${PATH_TO_OPERATOR}
      --tar-path=./${IMAGE_NAME}.tar
  artifacts:
    paths:
      - ./${IMAGE_NAME}.tar
  
test-built-image:
  stage: test-built-image
  image: docker:latest
  variables: !reference [get-dest-and-tag, variables]
  before_script:
    - docker load -i $IMAGE_NAME.tar
  script:
    - docker images
    - DESTINATIONS=$(cat destinations.txt)
    - FIRST_DEST=$(echo $DESTINATIONS | awk '{print $1}')
    - IMAGE_TO_TEST=$(echo $FIRST_DEST | sed 's/--destination=//g' | tr -d '"')
    - echo "image name is $IMAGE_TO_TEST"
    - docker run --rm -v /var/run/docker.sock:/var/run/docker.sock goodwithtech/dockle:v0.4.14 --exit-code 1 --exit-level fatal $IMAGE_TO_TEST
  tags: 
    - dind
  rules:
    !reference [get-dest-and-tag, rules]
  
push-image:
  stage: push-image
  image:
    name: alpine/crane
    entrypoint: [""]
  variables: !reference [get-dest-and-tag, variables]
  before_script:
    - apk add --no-cache jq
    - DOCKERHUB_AUTH_URL=$(echo "${DOCKERHUB_URL}" | sed 's|https://||;s|/.*||')
    - crane auth login -u ${REGISTRY_USER} -p ${REGISTRY_PASSWORD} ${REGISTRY_URL}
    - crane auth login -u ${DOCKERHUB_USER} -p ${DOCKERHUB_PASSWORD} ${DOCKERHUB_AUTH_URL}
    - crane auth login -u ${DOCKER_INTERNAL_REGISTRIES_USER} -p ${DOCKER_INTERNAL_REGISTRIES_PASSWORD} ${DOCKER_MIRROR_INTERNAL_REGISTRY_URL}
  script:
    - cat destinations.json
    - DESTINATION=$(jq -r '.destinations[]' destinations.json)
    - for dest in $DESTINATION; do
        echo "Pushing to $dest";
        crane push $IMAGE_NAME.tar $dest;
      done
  tags:
    - ovh-kosmos
  rules: 
    !reference [get-dest-and-tag, rules]


    
  environment:
    name: ${ENVIRONMENT}
    url: https://nexus.squashtest.org/nexus/

################# 
# Docker content trust jobs 

services: # needs DinD things
  - docker:23.0.5-dind

docker-start:
  # check if docker is correctly set and ready
  stage: docker:start
  image: docker:latest
  tags: 
    - dind
  before_script:
    - echo "Waiting for docker cli to respond before continuing build..."
    - |
      for i in $(seq 1 30); do
          if ! docker info &> /dev/null; then
              echo "Docker not responding yet. Sleeping for 2s..." && sleep 2s
          else
              echo "Docker ready. Continuing build..."
              break
          fi
      done
  script:
    - docker version
  rules:
    - if: '$IS_NIGHTLY == "true" && $CI_COMMIT_BRANCH == "main" && $CI_PIPELINE_SOURCE == "pipeline"'
      variables:
        TAG_LIST: "nightly"
        BUILD_TYPE: "nightly"
        ENVIRONMENT: development
      when: always
    - if: '$CI_COMMIT_TAG =~ /^v(\d{4})-(\d{2})\.(\d{2})$/ && $CI_COMMIT_REF_PROTECTED == "true"'
      variables:
        TAG_LIST: "${SHORT_TAG} ${DOCKER_TAG} ${LATEST}"
        BUILD_TYPE: "release"
      when: always
    - when: never

docker:sign:
  image: docker:dind
  stage: docker:signature
  retry: 2
  before_script:
    - sleep 3
    - echo $REGISTRY_PASSWORD | docker login --username $REGISTRY_USER --password-stdin
  variables:
    REGISTRY_URL: "${DOCKERHUB_URL}"
    REGISTRY_USER: "${DOCKERHUB_USER}"
    REGISTRY_PASSWORD : "${DOCKERHUB_PASSWORD}"
  script:
    - PATH_KEYS=$HOME/.docker/trust/private
    - mkdir -p $PATH_KEYS
    - chmod 600 $DCT_SIGNER_PRIV_KEY
    - cp $DCT_SIGNER_PRIV_KEY $PATH_KEYS/$SIGNER_KEY_NAME.key # GitLab CI variable type file
    - export DOCKER_CONTENT_TRUST_REPOSITORY_PASSPHRASE=$DCT_SIGNER_PASS # GitLab CI variable type variable
    - docker trust key load $PATH_KEYS/$SIGNER_KEY_NAME.key
    - echo $TAG_LIST
    - set -- $TAG_LIST
    - |-
      for TAG in "$@"; do
        echo "pull $TAG"
        docker pull ${IMAGE_PATH}/${IMAGE_NAME}:${TAG}
        echo sign
        docker trust sign ${IMAGE_PATH}/${IMAGE_NAME}:${TAG}
        echo push
        docker push ${IMAGE_PATH}/${IMAGE_NAME}:${TAG}
        docker trust inspect --pretty ${IMAGE_PATH}/${IMAGE_NAME}:${TAG}
      done
  after_script:
    - docker logout
    - PATH_KEYS=$HOME/.docker/trust/private
    - rm $PATH_KEYS/$SIGNER_KEY_NAME.key
  rules:
    - if: '$IS_NIGHTLY == "true" && $CI_COMMIT_BRANCH == "main" && $CI_PIPELINE_SOURCE == "pipeline"'
      variables:
        TAG_LIST: "nightly"
        BUILD_TYPE: "nightly"
        ENVIRONMENT: development
      when: always
    - if: '$CI_COMMIT_TAG =~ /^v(\d{4})-(\d{2})\.(\d{2})$/ && $CI_COMMIT_REF_PROTECTED == "true"'
      variables:
        TAG_LIST: "${SHORT_TAG} ${DOCKER_TAG} ${LATEST}"
        BUILD_TYPE: "release"
      when: always
    - when: never
  tags:
    - dind

trigger_cve-to-issues_pipeline:
  stage: test
  image: docker-group.squashtest.org/forge/git-jq-curl:latest
  script:
    - CURRENT_DATE=$(date +%Y-%m-%d)
    - >-
      curl --request POST 
      --form "token=$CI_JOB_TOKEN" 
      --form "ref=main" 
      --form "variables[scan_date]=$CURRENT_DATE" 
      --form "variables[project_name]=$IMAGE_NAME" 
      --form "variables[project_id]=$CI_PROJECT_ID" 
      --form "variables[project_fullpath]=$CI_PROJECT_PATH" 
      https://gitlab.com/api/v4/projects/52372362/trigger/pipeline
  only:
    - main
  needs: ["container_scanning"]
