ARG PYTHON_VERSION
FROM python:${PYTHON_VERSION}

ARG PATH_TO_OPERATOR

WORKDIR /src

COPY dist /src/dist

RUN pip install --no-cache-dir ./dist/*.whl

EXPOSE 8080

ENV PATH_TO_OPERATOR=${PATH_TO_OPERATOR}
ENV KOPF_OPTIONS="--liveness=http://0.0.0.0:8080/healthz --verbose"

CMD ["sh", "-c", "kopf run ${PATH_TO_OPERATOR} ${KOPF_OPTIONS}"]