# Copyright (c) 2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""operator unit tests"""

import base64
import json
import logging
import unittest

from collections import defaultdict
from queue import Queue
from unittest.mock import MagicMock, patch

import kopf
import requests

from opentf.operator import main


########################################################################
# Datasets

DEFAULT_SPEC = {
    "namespaces": ["default", "bar"],
    "orchestratorSecret": "supersecret",
    "poolSize": 2,
    "tags": ["linux", "robotframework", "customTag"],
    "template": {
        "metadata": {"labels": {"app": "my-app"}},
        "spec": {"containers": [{"image": "nginx:latest", "name": "my-container"}]},
    },
}

WO_NS_SPEC = {
    "orchestratorSecret": "supersecret",
    "poolSize": 2,
    "tags": ["linux", "robotframework", "customTag"],
    "template": {
        "metadata": {"labels": {"app": "my-app"}},
        "spec": {"containers": [{"image": "nginx:latest", "name": "my-container"}]},
    },
}

STATUS_THREE_AGENTS = {
    "create_agents": {
        "agents": ['uuid1', 'uuid2', 'uuid3'],
        "resource_id": "some_1234_resource_id",
    }
}

OS_ENV = {'ORCHESTRATOR_URL': 'localhost:42/foo/bar'}


########################################################################
# Helpers


def build_agentchannel_mock(response_json, status_code=200):
    response = requests.Response()
    response.status_code = status_code
    response.json = MagicMock(return_value=response_json)
    response._content = json.dumps(response_json).encode('utf-8')
    return response


def _make_copy(what):
    return json.loads(json.dumps(what))


########################################################################
# Tests


class TestOperatorMain(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    # event listeners OK

    def test_create_agents_ok(self):
        mock_logger = MagicMock()
        mock_post = MagicMock(
            return_value=build_agentchannel_mock(
                {'details': {'uuid': 'someUuid'}, 'message': 'agent registered'},
                status_code=201,
            )
        )
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value='tOkEn')
        ), patch('requests.post', mock_post), patch(
            'opentf.operator.main._start_threads'
        ) as mock_threads:
            result = main.create_agents('test-pool', 'default', DEFAULT_SPEC, mock_logger)  # type: ignore
        mock_threads.assert_called_once()
        self.assertEqual(DEFAULT_SPEC['poolSize'], len(result['agents']))
        self.assertTrue(result['resource_id'].startswith('test-pool'))
        self.assertEqual(4, mock_logger.debug.call_count)
        self.assertEqual('agent registered', mock_logger.debug.call_args_list[1][0][0])
        self.assertEqual(2, mock_post.call_count)
        self.assertEqual(
            'default,bar',
            mock_post.call_args_list[0][1]['json']['metadata']['namespaces'],
        )

    def test_create_agents_no_ns_ok(self):
        mock_logger = MagicMock()
        mock_post = MagicMock(
            return_value=build_agentchannel_mock(
                {'details': {'uuid': 'someUuid'}, 'message': 'agent registered'},
                status_code=201,
            )
        )
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value='tOkEn')
        ), patch('requests.post', mock_post), patch(
            'opentf.operator.main._start_threads'
        ) as mock_threads:
            main.create_agents('test-pool', 'default', WO_NS_SPEC, mock_logger)  # type: ignore
        mock_threads.assert_called_once()
        self.assertEqual(
            'default', mock_post.call_args_list[0][1]['json']['metadata']['namespaces']
        )

    def test_create_agents_ko_register_returns_422(self):
        mock_logger = MagicMock()
        mock_post = MagicMock(
            return_value=build_agentchannel_mock(
                {'message': 'Failed to register agent.'},
                status_code=422,
            )
        )
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value='tOkEn')
        ), patch('requests.post', mock_post), patch(
            'opentf.operator.main._start_threads'
        ):
            self.assertRaisesRegex(kopf.TemporaryError, 'Failed to register agent: error code 422,', main.create_agents, 'test-pool', 'default', DEFAULT_SPEC, mock_logger)  # type: ignore

    def test_create_agents_ko_negative_pool_size(self):
        self.assertRaisesRegex(
            kopf.TemporaryError,
            'Pool size must be a non-negative integer, got -2.',
            main.create_agents,
            'test-pool',
            'default',
            {'poolSize': -2},
            MagicMock(),
        )

    def test_delete_agents_ok(self):
        mock_logger = MagicMock()
        mock_delete = MagicMock(
            return_value=build_agentchannel_mock({'message': 'agent deregistered'})
        )
        mock_agents_pod = {'some_1234_resource_id': {'some-pod': 'no-pod'}}
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value='tOkEn')
        ), patch('requests.delete', mock_delete), patch(
            'opentf.operator.main._stop_threads'
        ) as mock_stop, patch(
            'opentf.operator.main._attempt_agent_removal'
        ) as mock_remove, patch(
            'opentf.operator.main.AGENTS_WITH_POD', mock_agents_pod
        ):
            main.delete_agents('some-pool', 'default', DEFAULT_SPEC, STATUS_THREE_AGENTS, mock_logger)  # type: ignore
        mock_stop.assert_called_once()
        mock_remove.assert_called_once()
        self.assertEqual({}, mock_agents_pod)

    def test_delete_agents_ko_deregister_no_status_agents(self):
        self.assertRaisesRegex(
            kopf.TemporaryError,
            'Failed to delete agents for pool "some-pool" in namespace "default"*',
            main.delete_agents,
            'some-pool',
            'default',
            {},
            {'create_agents': {'not_agents': 'no_deal'}},
            MagicMock(),
        )

    def test_relaunch_agents_ok(self):
        mock_logger = MagicMock()
        mock_get = MagicMock(
            return_value=build_agentchannel_mock(
                {
                    'items': [
                        {
                            'metadata': {
                                'agent_id': 'uuid1',
                                'name': 'test-pool-default-uuid1',
                            },
                            'status': {'phase': 'IDLE'},
                        }
                    ]
                }
            )
        )
        mock_post = MagicMock(
            return_value=build_agentchannel_mock(
                {'details': {'uuid': 'anotherUuid'}, 'message': 'agent registered'},
                status_code=201,
            )
        )

        mock_patch = MagicMock()
        mock_patch.status = _make_copy(STATUS_THREE_AGENTS)
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value=None)
        ), patch('requests.get', mock_get), patch('requests.post', mock_post), patch(
            'opentf.operator.main._start_threads'
        ) as mock_start_threads, patch(
            'opentf.operator.main.config.load_incluster_config'
        ):
            main.relaunch_agents('test-pool', 'default', DEFAULT_SPEC, STATUS_THREE_AGENTS, mock_patch, mock_logger)  # type: ignore
        mock_get.assert_called_once()
        mock_start_threads.assert_called_once()
        self.assertEqual(1, mock_post.call_count)
        self.assertEqual(
            ['uuid1', 'anotherUuid'],
            mock_patch.status['create_agents']['agents'],
        )

    def test_relaunch_agents_create(self):
        mock_logger = MagicMock()
        mock_get = MagicMock(
            return_value=build_agentchannel_mock(
                {
                    'items': [
                        {
                            'metadata': {
                                'agent_id': 'uuid1',
                                'name': 'test-pool-default-uuid1',
                            },
                            'status': {'phase': 'IDLE'},
                        }
                    ]
                }
            )
        )
        mock_post = MagicMock(
            return_value=build_agentchannel_mock(
                {'details': {'uuid': 'recreateUuid'}, 'message': 'agent registered'},
                status_code=201,
            )
        )
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value=None)
        ), patch('requests.get', mock_get), patch('requests.post', mock_post), patch(
            'opentf.operator.main.config.load_incluster_config'
        ), patch(
            'opentf.operator.main._register_agents'
        ) as mock_register_agents, patch(
            'opentf.operator.main._start_threads'
        ) as mock_start_threads:
            main.relaunch_agents('test-pool', 'default', DEFAULT_SPEC, {}, MagicMock(), mock_logger)  # type: ignore
        self.assertEqual(1, mock_register_agents.call_args[0][0])
        mock_get.assert_called_once()
        mock_start_threads.assert_called_once()

    def test_relaunch_agents_ok_get_returns_empty(self):
        mock_logger = MagicMock()
        mock_get = MagicMock(
            return_value=build_agentchannel_mock({'items': []}, status_code=200)
        )
        mock_post = MagicMock(
            return_value=build_agentchannel_mock(
                {'details': {'uuid': 'recreateUuid'}, 'message': 'agent registered'},
                status_code=201,
            )
        )
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value=None)
        ), patch('requests.get', mock_get), patch('requests.post', mock_post), patch(
            'opentf.operator.main._start_threads'
        ) as mock_start_threads, patch(
            'opentf.operator.main.config.load_incluster_config'
        ), patch(
            'opentf.operator.main._register_agents'
        ) as mock_register:
            main.relaunch_agents('test-pool', 'default', DEFAULT_SPEC, {}, MagicMock(), mock_logger)  # type: ignore
        mock_start_threads.assert_called_once()
        mock_get.assert_called_once()
        mock_register.assert_called_once()
        self.assertEqual(2, mock_register.call_args[0][0])

    def test_relaunch_agents_ko_get_returns_500(self):
        mock_logger = MagicMock()
        mock_get = MagicMock(
            return_value=build_agentchannel_mock({'message': 'Bam.'}, status_code=500)
        )
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value=None)
        ), patch('requests.get', mock_get), patch(
            'opentf.operator.main._start_threads'
        ), patch(
            'opentf.operator.main.config.load_incluster_config'
        ):
            self.assertRaisesRegex(kopf.TemporaryError, 'Failed to resume agents for pool "test-pool" in namespace "default": Cannot retrieve registered agents list: Bam.*', main.relaunch_agents, 'test-pool', 'default', DEFAULT_SPEC, {}, MagicMock(), mock_logger)  # type: ignore
        mock_get.assert_called_once()

    def test_relaunch_agents_deregister_busy_delete_pod(self):
        mock_logger = MagicMock()
        mock_get = MagicMock(
            return_value=build_agentchannel_mock(
                {
                    'items': [
                        {
                            'metadata': {
                                'agent_id': 'uuid1',
                                'name': 'test-pool-default-uuid1',
                            },
                            'status': {'phase': 'IDLE'},
                        },
                        {
                            'metadata': {
                                'agent_id': 'uuid2',
                                'name': 'test-pool-default-uuid2',
                            },
                            'status': {'phase': 'BUSY'},
                        },
                    ]
                }
            )
        )
        mock_post = MagicMock(
            return_value=build_agentchannel_mock(
                {'details': {'uuid': 'anotherUuid'}, 'message': 'agent registered'},
                status_code=201,
            )
        )

        mock_patch = MagicMock()
        status_dict = _make_copy(STATUS_THREE_AGENTS)
        status_dict['create_agents']['agents_pods'] = {'uuid2': 'pod-uuid2'}
        mock_patch.status = status_dict
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value=None)
        ), patch('requests.get', mock_get), patch('requests.post', mock_post), patch(
            'opentf.operator.main._start_threads'
        ) as mock_threads, patch(
            'opentf.operator.main.config.load_incluster_config'
        ), patch(
            'opentf.operator.main._patch_pools'
        ), patch(
            'opentf.operator.main._deregister_agent'
        ) as mock_deregister, patch(
            'opentf.operator.main._delete_exec_pod'
        ) as mock_delete_pod:
            main.relaunch_agents('test-pool', 'default', DEFAULT_SPEC, status_dict, mock_patch, mock_logger)  # type: ignore
        mock_threads.assert_called_once()
        mock_deregister.assert_called_once()
        self.assertEqual('uuid2', mock_deregister.call_args[0][0])
        mock_delete_pod.assert_called_once()
        self.assertEqual('pod-uuid2', mock_delete_pod.call_args[0][0])

    # update_pool_size

    def test_update_pool_size_ok_add(self):
        diff = (('change', 'foo', 3, 7),)
        mock_logger = MagicMock()
        mock_post = MagicMock(
            return_value=build_agentchannel_mock(
                {'details': {'uuid': 'updateUuid'}, 'message': 'agent registered'},
                status_code=201,
            )
        )
        mock_patch = MagicMock()
        mock_patch.status = _make_copy(STATUS_THREE_AGENTS)
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value='tOken')
        ), patch('requests.post', mock_post):
            main.update_pool_size('test-pool', 'default', DEFAULT_SPEC, STATUS_THREE_AGENTS, mock_patch, diff, mock_logger)  # type: ignore
        self.assertEqual(4, mock_post.call_count)
        self.assertEqual(7, len(mock_patch.status['create_agents']['agents']))

    def test_update_pool_size_ok_delete(self):
        diff = (('change', 'foo', 3, 0),)
        mock_logger = MagicMock()
        mock_delete = MagicMock(
            return_value=build_agentchannel_mock(
                {'message': 'agent deleted'},
                status_code=200,
            )
        )
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value='tOken')
        ), patch('requests.delete', mock_delete), patch(
            'opentf.operator.main.AGENTS_WITH_POD', {'some_1234_resource_id': set()}
        ), patch(
            'opentf.operator.main._patch_pools'
        ) as mock_patch:
            main.update_pool_size('test-pool', 'default', DEFAULT_SPEC, STATUS_THREE_AGENTS, mock_patch, diff, mock_logger)  # type: ignore
        self.assertEqual(3, mock_delete.call_count)
        self.assertEqual(0, len(mock_patch.status['create_agents']['agents']))

    def test_update_pool_size_ok_delete_busy_agent(self):
        diff = (('change', 'foo', 3, 0),)
        mock_logger = MagicMock()
        mock_delete = MagicMock(
            return_value=build_agentchannel_mock(
                {'message': 'agent deleted'},
                status_code=200,
            )
        )
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value='tOken')
        ), patch('requests.delete', mock_delete), patch(
            'opentf.operator.main.AGENTS_WITH_POD', {'some_1234_resource_id': {'uuid1'}}
        ), patch(
            'opentf.operator.main._patch_pools'
        ) as mock_patch, patch(
            'opentf.operator.main._attempt_agent_removal'
        ) as mock_remove:
            main.update_pool_size('test-pool', 'default', DEFAULT_SPEC, STATUS_THREE_AGENTS, mock_patch, diff, mock_logger)  # type: ignore
        mock_remove.assert_called_once()

    def test_update_pool_size_ko_no_pod(self):
        diff = (('change', 'foo', 3, 0),)
        self.assertRaisesRegex(
            kopf.TemporaryError,
            '''Failed to update pool size for pool "some-name" in namespace "default": 'Failed to get resource id*''',
            main.update_pool_size,
            'some-name',
            'default',
            {},
            {},
            MagicMock(),
            diff,
            MagicMock(),
        )

    def test_update_tags_ok(self):
        diff = (('change', 'bar', ['tag1'], ['tag1', 'tag2']),)
        mock_logger = MagicMock()
        mock_delete = MagicMock(
            return_value=build_agentchannel_mock({'message': 'agent deleted'})
        )
        mock_post = MagicMock(
            return_value=build_agentchannel_mock(
                {'details': {'uuid': 'newTagsUuid'}, 'message': 'agent registered'},
                status_code=201,
            )
        )
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value=None)
        ), patch('requests.delete', mock_delete), patch(
            'requests.post', mock_post
        ), patch(
            'opentf.operator.main.AGENTS_WITH_POD', {'some_1234_resource_id': {}}
        ), patch(
            'opentf.operator.main._patch_pools'
        ) as mock_patch:
            main.update_tags('test-pool', 'default', DEFAULT_SPEC, STATUS_THREE_AGENTS, diff, mock_logger)  # type: ignore
        self.assertEqual(3, mock_delete.call_count)
        self.assertEqual(3, mock_post.call_count)
        mock_patch.assert_called_once()
        self.assertEqual(
            ['tag1', 'tag2'],
            mock_patch.call_args[0][2]['status']['create_agents']['tags'],
        )
        self.assertEqual(
            {'newTagsUuid'},
            set(mock_patch.call_args[0][2]['status']['create_agents']['agents']),
        )

    def test_update_tags_ko_deregister_returns_404(self):
        diff = (('change', 'bar', ['tag1'], ['tag1', 'tag2']),)
        mock_logger = MagicMock()
        mock_delete = MagicMock(
            return_value=build_agentchannel_mock(
                {'message': 'agent not deleted'}, status_code=404
            )
        )
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value=None)
        ), patch('requests.delete', mock_delete), patch(
            'opentf.operator.main._patch_pools'
        ):
            self.assertRaisesRegex(kopf.TemporaryError, 'Failed to update agents tags for pool "test-pool" in namespace "default": Cannot de-register agent "uuid1": agent not deleted*', main.update_tags, 'test-pool', 'default', DEFAULT_SPEC, STATUS_THREE_AGENTS, diff, mock_logger)  # type: ignore

    # _make_headers

    def test_make_headers_ok(self):
        secret = MagicMock()
        secret.data = {'token': base64.b64encode(b'tOkeN').decode('utf-8')}
        mock_api = MagicMock()
        mock_api_instance = mock_api.return_value
        mock_api_instance.read_namespaced_secret.return_value = secret
        mock_logger = MagicMock()
        with patch('opentf.operator.main.config.load_incluster_config'), patch(
            'opentf.operator.main.client.CoreV1Api', mock_api
        ):
            headers = main._make_headers(
                {'orchestratorSecret': 'wroomwroom'}, '', mock_logger
            )
        self.assertEqual({'Authorization': 'Bearer tOkeN'}, headers)

    def test_make_headers_nosecret(self):
        self.assertIsNone(main._make_headers({}, '', MagicMock()))

    def test_make_header_exception(self):
        mock_logger = MagicMock()
        mock_api = MagicMock()
        mock_api.instance = mock_api.return_value
        mock_api.instance.read_namespaced_secret.side_effect = Exception('Booo')
        with patch('opentf.operator.main.config.load_incluster_config'), patch(
            'opentf.operator.main.client.CoreV1Api', mock_api
        ):
            main._make_headers(
                {'orchestratorSecret': '110011'},
                '',
                mock_logger,
            )
        mock_logger.error.assert_called_once_with('Booo')

    # monitor_agents

    def test_monitor_agents_ok(self):
        mock_logger = MagicMock()
        mock_get_logger = MagicMock()
        mock_get_logger.return_value = mock_logger
        mock_logger.error.side_effect = Exception('Wroom')
        status = {
            'status': {
                'create_agents': {'agents': ['agent1'], 'resource_id': 'some_details'}
            }
        }
        mock_api = MagicMock()
        mock_api_instance = mock_api.return_value
        mock_api_instance.get_namespaced_custom_object.return_value = status
        stop_event = MagicMock()
        stop_event.is_set.side_effect = [False, False]
        mock_queue = MagicMock()
        mock_dict = {'some-pool.default': mock_queue}
        mock_agents = MagicMock(
            return_value=[
                {'metadata': {'agent_id': 'agent1'}, 'status': {'phase': 'BUSY'}}
            ]
        )
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value='t0ken')
        ), patch('opentf.operator.main.config.load_incluster_config'), patch(
            'opentf.operator.main.client.CustomObjectsApi', mock_api
        ), patch(
            'time.sleep', MagicMock(side_effect=[None, Exception('Boom')])
        ), patch(
            'opentf.operator.main.logging.getLogger', mock_get_logger
        ), patch(
            'opentf.operator.main._get_agents', mock_agents
        ), patch(
            'opentf.operator.main.ACTIVE_AGENTS', mock_dict
        ):
            self.assertRaisesRegex(
                Exception,
                'Wroom',
                main.monitor_agents,
                stop_event,
                'some-pool',
                'default',
                DEFAULT_SPEC,
            )
        mock_queue.put.assert_called_once()
        self.assertIn('agent1' and 'some_details', mock_queue.put.call_args[0][0])
        mock_logger.info.assert_called_once()

    def test_monitor_agents_ok_no_agents(self):
        mock_logger = MagicMock()
        mock_get_logger = MagicMock()
        mock_get_logger.return_value = mock_logger
        mock_logger.error.side_effect = Exception('Rwoom')
        mock_sleep = MagicMock()
        mock_sleep.side_effect = [None, Exception('Boom')]
        status = {'create_agents': {'agents': []}, 'resource_id': 'some_id'}
        stop_event = MagicMock()
        stop_event.is_set.side_effect = [False, False]
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value='t0ken')
        ), patch('time.sleep', mock_sleep), patch(
            'opentf.operator.main.logging.getLogger', mock_get_logger
        ), patch(
            'opentf.operator.main._get_live_status', MagicMock(return_value=status)
        ):
            self.assertRaisesRegex(
                Exception,
                'Rwoom',
                main.monitor_agents,
                stop_event,
                'name',
                'default',
                DEFAULT_SPEC,
            )
        mock_logger.debug.assert_called_once_with(
            'No agents found for resource. Will retry.'
        )

    def test_monitor_agents_ok_old_orchestrator_version(self):
        mock_logger = MagicMock()
        mock_get_logger = MagicMock()
        mock_get_logger.return_value = mock_logger
        mock_logger.error.side_effect = Exception('Wroom')
        status = {
            'status': {
                'create_agents': {
                    'agents': ['agent1', 'agent42'],
                    'orchestrator': '0.65.0.dev.or.not.dev',
                }
            }
        }
        mock_api = MagicMock()
        mock_api_instance = mock_api.return_value
        mock_api_instance.get_namespaced_custom_object.return_value = status
        mock_get = MagicMock(
            side_effect=[
                build_agentchannel_mock({}, 204),
                build_agentchannel_mock({'details': 'some_details'}),
            ]
        )
        stop_event = MagicMock()
        stop_event.is_set.side_effect = [False, False]
        mock_queue = MagicMock()
        mock_dict = {'default': mock_queue}
        mock_agents = MagicMock(
            return_value=[
                {'metadata': {'agent_id': 'agent1'}, 'status': {'phase': 'IDLE'}},
                {'metadata': {'agent_id': 'agent42'}, 'status': {'phase': 'BUSY'}},
            ]
        )
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', MagicMock(return_value='t0ken')
        ), patch('opentf.operator.main.AGENTS_COMMANDS', mock_dict), patch(
            'requests.get', mock_get
        ), patch(
            'opentf.operator.main.config.load_incluster_config'
        ), patch(
            'opentf.operator.main.client.CustomObjectsApi', mock_api
        ), patch(
            'time.sleep', MagicMock(side_effect=[None, Exception('Boom')])
        ), patch(
            'opentf.operator.main.logging.getLogger', mock_get_logger
        ), patch(
            'opentf.operator.main._get_agents', mock_agents
        ):
            self.assertRaisesRegex(
                Exception,
                'Wroom',
                main.monitor_agents,
                stop_event,
                'some-pool',
                'default',
                DEFAULT_SPEC,
            )

    def test_monitor_agents_ko_url_not_found(self):
        with patch(
            'opentf.operator.main._get_orchestrator_url',
            MagicMock(side_effect=main.NotFound('Boom')),
        ):
            self.assertRaises(
                main.ThreadError, main.monitor_agents, 'foo', 'bar', 'foobar', {}
            )

    def test_monitor_agents_orchestrator_ko(self):
        mock_logger = MagicMock()
        mock_get_logger = MagicMock()
        mock_get_logger.return_value = mock_logger
        mock_logger.error.side_effect = Exception('Wroom')
        status = {
            'create_agents': {'agents': ['agent111'], 'resource_id': 'some_details_1'}
        }
        stop_event = MagicMock()
        stop_event.is_set.side_effect = [False, False]
        mock_agents = MagicMock(return_value=[])
        agents_pods = {
            'some_details_1': {'agent111': {'pod': 'pod1', 'command': False}}
        }
        with patch.dict('os.environ', OS_ENV), patch(
            'opentf.operator.main._make_headers', return_value='t0ken'
        ), patch('opentf.operator.main.config.load_incluster_config'), patch(
            'time.sleep', MagicMock(side_effect=[None, Exception('Boom')])
        ), patch(
            'opentf.operator.main.logging.getLogger', mock_get_logger
        ), patch(
            'opentf.operator.main._get_agents', mock_agents
        ), patch(
            'opentf.operator.main._get_live_status', return_value=status
        ), patch(
            'opentf.operator.main.AGENTS_WITH_POD', agents_pods
        ), patch(
            'opentf.operator.main._delete_exec_pod'
        ) as mock_delete_pod, patch(
            'opentf.operator.main._patch_pools'
        ) as mock_patch, patch(
            'opentf.operator.main._register_agents'
        ) as mock_register:
            self.assertRaisesRegex(
                Exception,
                'Wroom',
                main.monitor_agents,
                stop_event,
                'some-pool',
                'default',
                DEFAULT_SPEC,
            )
        mock_delete_pod.assert_called_once_with('pod1', 'default', mock_logger)
        self.assertEqual(2, mock_patch.call_count)
        mock_register.assert_called_once_with(
            2, 'some-pool', 'default', DEFAULT_SPEC, mock_logger
        )
        self.assertEqual({}, agents_pods)
        mock_logger.warn.assert_called_once()

    # handle_agent_command

    def test_handle_agent_command_ok(self):
        mock_logger = MagicMock()
        mock_get_logger = MagicMock()
        mock_get_logger.return_value = mock_logger
        mock_queue = Queue()
        mock_queue.put(
            (
                'agent1',
                {'kind': 'put'},
                'some-pool',
                'default',
                DEFAULT_SPEC,
                mock_logger,
            )
        )
        mock_queue.put(
            (
                'agent1',
                {'kind': 'exec', 'command': '-2.sh'},
                'some-pool',
                'default',
                DEFAULT_SPEC,
                mock_logger,
            )
        )
        commands = {'pool.default': mock_queue}
        stop_event = MagicMock()
        stop_event.is_set.side_effect = [False, False, True]
        with patch('opentf.operator.main.logging.getLogger', mock_get_logger), patch(
            'opentf.operator.main.config.load_incluster_config'
        ), patch('opentf.operator.main.dispatch') as mock_dispatch:
            main.handle_agent_command(stop_event, 'pool', 'default', commands, 10)
        mock_dispatch.assert_called()
        self.assertEqual(2, mock_dispatch.call_count)

    def test_handle_agent_command_ko_cannot_create_pod(self):
        mock_logger = MagicMock()
        mock_get_logger = MagicMock()
        mock_get_logger.return_value = mock_logger
        mock_core_api = MagicMock()
        mock_object_api = MagicMock()
        mock_token = MagicMock()
        mock_token.side_effect = [None, None]
        mock_gc = MagicMock(return_value=False)
        commands = {'pool.default': Queue()}
        commands['pool.default'].put(
            (
                'agent1',
                {'kind': 'put'},
                'some-pool',
                'default',
                DEFAULT_SPEC,
                mock_logger,
            )
        )
        busy_agents = defaultdict(set)
        stop_event = MagicMock()
        stop_event.is_set.side_effect = [False, True]
        status = {'create_agents': {'resource_id': 'some_id', 'agents': []}}
        with patch('opentf.operator.main.config.load_incluster_config'), patch(
            'opentf.operator.main.client.CoreV1Api', mock_core_api
        ), patch(
            'opentf.operator.main.client.CustomObjectsApi', mock_object_api
        ), patch(
            'opentf.operator.main._make_headers', mock_token
        ), patch.dict(
            'os.environ', OS_ENV
        ), patch(
            'opentf.operator.main.AGENTS_COMMANDS', commands
        ), patch(
            'opentf.operator.main.AGENTS_WITH_POD', busy_agents
        ), patch(
            'time.sleep'
        ), patch(
            'opentf.operator.main._maybe_get_agent_command', mock_gc
        ), patch(
            'opentf.operator.main._deregister_agent'
        ), patch(
            'opentf.operator.main._register_agents',
            MagicMock(return_value=['uuid3']),
        ), patch(
            'opentf.operator.main._create_exec_pod',
            MagicMock(side_effect=main.PodError('Fail')),
        ), patch(
            'opentf.operator.main._patch_pools'
        ), patch(
            'opentf.operator.main._get_live_status', MagicMock(return_value=status)
        ):
            main.handle_agent_command(stop_event, 'pool', 'default', commands, 10)
        mock_logger.error.assert_called_once_with('Fail')

    def test_handle_agent_command_ko_exception(self):
        mock_logger = MagicMock()
        mock_logger.error.side_effect = Exception('Wrrr')
        mock_get_logger = MagicMock()
        mock_get_logger.return_value = mock_logger
        stop_event = MagicMock()
        stop_event.is_set.side_effect = [False]
        bangbang = Exception('Bang-bang')
        mock_cmd = MagicMock()
        mock_cmd.get.side_effect = bangbang
        mock_queue = {'pool-some.not-default': mock_cmd}
        with patch('opentf.operator.main.logging.getLogger', mock_get_logger):
            self.assertRaisesRegex(
                Exception,
                'Wrrr',
                main.handle_agent_command,
                stop_event,
                'pool-some',
                'not-default',
                mock_queue,
                10,
            )
        mock_logger.error.assert_called_once_with(
            'Error in agents commands handling thread: %s.', bangbang
        )

    # dispatch

    def test_dispatch_ok_no_pod(self):
        mock_logger = MagicMock()
        mock_api = MagicMock()
        mock_core_api = MagicMock()
        mock_core_api.return_value = mock_api
        mock_token = MagicMock()
        mock_token.side_effect = [None, None]
        status = {'create_agents': {'resource_id': 'some_id'}}
        command = {'kind': 'run', 'command': 'foo'}
        agents_pod = defaultdict(dict)
        mock_run = MagicMock()
        mock_kind = {'run': mock_run}
        with patch('opentf.operator.main.config.load_incluster_config'), patch(
            'opentf.operator.main.client.CoreV1Api', mock_core_api
        ), patch('opentf.operator.main._make_headers', mock_token), patch.dict(
            'os.environ', OS_ENV
        ), patch(
            'opentf.operator.main._get_live_status', MagicMock(return_value=status)
        ), patch(
            'opentf.operator.main._patch_pools'
        ) as mock_patch, patch(
            'opentf.operator.main.AGENTS_WITH_POD', agents_pod
        ), patch(
            'opentf.operator.main.KIND_HANDLERS', mock_kind
        ):
            main.dispatch(
                'agent', command, 'name', 'default', DEFAULT_SPEC, mock_logger
            )
        self.assertEqual(
            {'some_id': {'agent': {'command': False, 'pod': 'name-agent'}}}, agents_pod
        )
        mock_patch.assert_called_once()
        mock_run.assert_called_once()
        mock_api.create_namespaced_pod.assert_called_once()

    def test_dispatch_ok_delete_pod(self):
        mock_logger = MagicMock()
        mock_api = MagicMock()
        mock_core_api = MagicMock()
        mock_core_api.return_value = mock_api
        mock_token = MagicMock()
        mock_token.side_effect = [None, None]
        status = {'create_agents': {'resource_id': 'some_id'}}
        command = {'kind': 'run', 'command': '-2.sh'}
        agents_pod = {'some_id': {'agent': 'blah'}}
        mock_run = MagicMock()
        mock_kind = {'run': mock_run}
        with patch('opentf.operator.main.config.load_incluster_config'), patch(
            'opentf.operator.main.client.CoreV1Api', mock_core_api
        ), patch('opentf.operator.main._make_headers', mock_token), patch.dict(
            'os.environ', OS_ENV
        ), patch(
            'opentf.operator.main._get_live_status', MagicMock(return_value=status)
        ), patch(
            'opentf.operator.main._patch_pools'
        ) as mock_patch, patch(
            'opentf.operator.main.AGENTS_WITH_POD', agents_pod
        ), patch(
            'opentf.operator.main.KIND_HANDLERS', mock_kind
        ), patch(
            'time.sleep'
        ), patch(
            'opentf.operator.main._maybe_get_agent_command',
            MagicMock(return_value=False),
        ):
            main.dispatch(
                'agent', command, 'name', 'default', DEFAULT_SPEC, mock_logger
            )
        self.assertEqual({'some_id': {}}, agents_pod)
        mock_patch.assert_called_once()
        mock_run.assert_called_once()
        mock_api.delete_namespaced_pod.assert_called_once()

    def test_dispatch_ko_exception(self):
        mock_logger = MagicMock()
        mock_api = MagicMock()
        mock_core_api = MagicMock()
        mock_core_api.return_value = mock_api
        mock_token = MagicMock()
        with patch('opentf.operator.main.config.load_incluster_config'), patch(
            'opentf.operator.main.client.CoreV1Api', mock_core_api
        ), patch('opentf.operator.main._make_headers', mock_token), patch.dict(
            'os.environ', OS_ENV
        ), patch(
            'opentf.operator.main._get_live_status', side_effect=Exception('Boom')
        ):
            main.dispatch('agent', {}, 'name', 'default', {}, mock_logger)
        mock_logger.error.assert_called_once_with(
            'Error while handling agent commands: %s.', 'Boom'
        )

    # handle active agents

    def test_handle_active_agents_ok(self):
        mock_logger = MagicMock()
        mock_get_logger = MagicMock()
        mock_get_logger.return_value = mock_logger
        active_agents = {'name.default': Queue()}
        active_agents['name.default'].put(('agent1', 'some_id'))
        active_agents['name.default'].put(('agent2', 'some_id'))
        stop_event = MagicMock()
        stop_event.is_set.side_effect = [False, False, True]
        mock_agents_pod = {
            'some_id': {'agent1': {'command': True}, 'agent2': {'command': False}}
        }
        mock_cmd_queue = MagicMock()
        mock_commands = {'name.default': mock_cmd_queue}
        with patch('opentf.operator.main.logging.getLogger', mock_get_logger), patch(
            'opentf.operator.main.ACTIVE_AGENTS', active_agents
        ), patch('opentf.operator.main.AGENTS_WITH_POD', mock_agents_pod), patch(
            'opentf.operator.main._maybe_get_agent_command', return_value=True
        ) as mock_get_cmd, patch(
            'opentf.operator.main.AGENTS_COMMANDS', mock_commands
        ):
            main.handle_active_agents(stop_event, 'name', 'default', {})
        mock_get_cmd.assert_called_once()
        mock_cmd_queue.put.assert_called_once()
        self.assertTrue(mock_agents_pod['some_id']['agent2']['command'])

    def test_handle_active_agents_ko_exception(self):
        boom = Exception('Boom')
        wroom = Exception('Wroom')
        mock_logger = MagicMock()
        mock_logger.error.side_effect = wroom
        mock_get_logger = MagicMock()
        mock_get_logger.return_value = mock_logger
        stop_event = MagicMock()
        stop_event.is_set.side_effect = [False]
        mock_agents = MagicMock()
        mock_agents.get.side_effect = boom
        active_agents = {'name.default': mock_agents}
        with patch('opentf.operator.main.logging.getLogger', mock_get_logger), patch(
            'opentf.operator.main.ACTIVE_AGENTS', active_agents
        ):
            self.assertRaisesRegex(
                Exception,
                'Wroom',
                main.handle_active_agents,
                stop_event,
                'name',
                'default',
                {},
            )
        mock_logger.error.assert_called_once_with(
            'Error in active agents handling thread: %s.', boom
        )

    # _remove_agent_and_pod

    def test_remove_agent_and_pod_ok(self):
        mock_pod = {'resource_id': {'uuid1': {}, 'uuid2': {}, 'uuid3': {}}}
        mock_status = MagicMock(
            return_value={
                'create_agents': {
                    'agents_pods': {'uuid1': 'pod1', 'uuid2': 'pod2', 'uuid3': 'pod3'}
                }
            }
        )
        with patch('opentf.operator.main.AGENTS_WITH_POD', mock_pod), patch(
            'opentf.operator.main._get_live_status', mock_status
        ), patch('opentf.operator.main._patch_pools') as mock_patch, patch(
            'opentf.operator.main._delete_exec_pod'
        ) as mock_delete:
            main._remove_agent_and_pod(
                'uuid2', 'resource_id', 'name', 'default', 'pod2', 'bar'
            )
        mock_patch.assert_called_once()
        self.assertIsNone(
            mock_patch.call_args[0][2]['status']['create_agents']['agents_pods'][
                'uuid2'
            ]
        )
        mock_delete.assert_called_once_with('pod2', 'default', 'bar')

    def test_remove_agent_and_pod_keyerror(self):
        mock_pod = {'not_resource_id': {}}
        with patch('opentf.operator.main._delete_exec_pod') as mock_delete, patch(
            'opentf.operator.main.AGENTS_WITH_POD', mock_pod
        ):
            main._remove_agent_and_pod(
                'uuid_x', 'resource_id', 'name', 'default', 'pod2', 'bar'
            )
        mock_delete.assert_called_once_with('pod2', 'default', 'bar')

    # process put, get, exec

    def test_process_get_cmd_ok(self):
        mock_logger = MagicMock()
        mock_execute = MagicMock(return_value=('file', '', 0))
        mock_post = MagicMock(return_value=build_agentchannel_mock({}, 200))
        with patch(
            'opentf.operator.main._is_pod_running', MagicMock(return_value=True)
        ), patch('opentf.operator.main._execute_cmd_on_pod', mock_execute), patch(
            'requests.post', mock_post
        ):
            main._process_get_cmd(
                'url',
                {'path': 'foo.txt', 'file_id': 'fileId'},
                None,
                'sample-pod',
                'default',
                MagicMock(),
                mock_logger,
            )
        mock_execute.assert_called_once()
        self.assertEqual('base64 foo.txt', mock_execute.call_args[0][0])
        self.assertEqual('sample-pod', mock_execute.call_args[0][1])
        mock_post.assert_called_once_with(
            'url/files/fileId', data=b'~)^', headers=None, timeout=60
        )

    def test_process_get_cmd_ko_poderror(self):
        mock_logger = MagicMock()
        mock_execute = MagicMock(return_value=('file', 'big error', 1))
        mock_post = MagicMock(return_value=build_agentchannel_mock({}, 200))
        with patch(
            'opentf.operator.main._is_pod_running', MagicMock(return_value=True)
        ), patch('opentf.operator.main._execute_cmd_on_pod', mock_execute), patch(
            'requests.post', mock_post
        ):
            main._process_get_cmd(
                'url',
                {'path': 'foo.txt', 'file_id': 'fileId'},
                None,
                'sample-pod',
                'default',
                MagicMock(),
                mock_logger,
            )
        mock_post.assert_called_once()
        self.assertIn(
            '"foo.txt": big error', mock_post.call_args[1]['json']['details']['error']
        )

    def test_process_get_cmd_ko_timeout_failed_to_post(self):
        mock_logger = MagicMock()
        mock_post = MagicMock(return_value=build_agentchannel_mock({}, 404))
        with patch(
            'opentf.operator.main._is_pod_running', MagicMock(return_value=False)
        ), patch('requests.post', mock_post):
            main._process_get_cmd(
                'url',
                {'path': 'foo.txt', 'file_id': 'fileId'},
                None,
                'sample-pod',
                'default',
                MagicMock(),
                mock_logger,
            )
        mock_post.assert_called_once()
        mock_logger.error.assert_called_once_with(
            'Failed to push command result.  Got a %d status code.', 404
        )

    def test_process_get_cmd_ko_no_path(self):
        mock_logger = MagicMock()
        self.assertIsNone(
            main._process_get_cmd(
                'url', {}, None, 'sample-pod', 'default', MagicMock(), mock_logger
            )
        )
        mock_logger.error.assert_called_once_with('No path specified in command.')

    def test_process_get_cmd_ko_no_file_id(self):
        mock_logger = MagicMock()
        self.assertIsNone(
            main._process_get_cmd(
                'url',
                {'path': 'path'},
                None,
                'sample-pod',
                'default',
                MagicMock(),
                mock_logger,
            )
        )
        mock_logger.error.assert_called_once_with('No file_id specified in command.')

    def test_process_exec_cmd_ok(self):
        mock_logger = MagicMock()
        mock_execute = MagicMock(return_value=('Executed', 'And failed', 0))
        mock_post = MagicMock(return_value=build_agentchannel_mock({}, 200))
        with patch(
            'opentf.operator.main._is_pod_running', MagicMock(return_value=True)
        ), patch('opentf.operator.main._execute_cmd_on_pod', mock_execute), patch(
            'requests.post', mock_post
        ):
            main._process_exec_cmd(
                'url',
                {'command': 'echo foo'},
                None,
                'sample-pod',
                'default',
                MagicMock(),
                mock_logger,
            )
        mock_execute.assert_called_once()
        mock_post.assert_called_once()
        self.assertEqual(
            {'stdout': ['Executed'], 'stderr': ['And failed'], 'exit_status': 0},
            mock_post.call_args[1]['json'],
        )

    def test_process_exec_cmd_ko_failed_to_push(self):
        mock_logger = MagicMock()
        mock_execute = MagicMock(return_value=('Executed', 'And failed', 0))
        mock_post = MagicMock(return_value=build_agentchannel_mock({}, 404))
        with patch(
            'opentf.operator.main._is_pod_running', MagicMock(return_value=True)
        ), patch('opentf.operator.main._execute_cmd_on_pod', mock_execute), patch(
            'requests.post', mock_post
        ):
            main._process_exec_cmd(
                'url',
                {'command': 'echo foo'},
                None,
                'sample-pod',
                'default',
                MagicMock(),
                mock_logger,
            )
        mock_logger.error.assert_called_once_with(
            'Failed to push command result.  Got a %d status code.', 404
        )

    def test_process_exec_cmd_ko_failed_to_push_exception(self):
        fail = Exception('Fail!')
        mock_logger = MagicMock()
        mock_execute = MagicMock(return_value=('Executed', 'And failed', 0))
        mock_post = MagicMock(side_effect=[fail, build_agentchannel_mock({}, 200)])
        with patch(
            'opentf.operator.main._is_pod_running', MagicMock(return_value=True)
        ), patch('opentf.operator.main._execute_cmd_on_pod', mock_execute), patch(
            'requests.post', mock_post
        ):
            main._process_exec_cmd(
                'url',
                {'command': 'echo foo'},
                None,
                'sample-pod',
                'default',
                MagicMock(),
                mock_logger,
            )
        mock_logger.error.assert_called_once_with(
            'Failed to push command result: %s.  Retrying.', fail
        )

    def test_process_exec_cmd_ko_timeout(self):
        timeout = TimeoutError('Timed out, pod still not running')
        mock_logger = MagicMock()
        with patch(
            'opentf.operator.main._is_pod_running', MagicMock(return_value=False)
        ), patch('opentf.operator.main.TimeoutError', MagicMock(return_value=timeout)):
            main._process_exec_cmd(
                'url',
                {'command': 'some cmd'},
                None,
                'sample-pod',
                'default',
                MagicMock(),
                mock_logger,
            )
        mock_logger.error.assert_called_once_with(
            'Failed to execute command: %s.', timeout
        )

    def test_process_put_cmd_ok(self):
        mock_logger = MagicMock()
        with patch(
            'opentf.operator.main._is_pod_running', MagicMock(return_value=True)
        ), patch('opentf.operator.main._upload_file_to_pod') as mock_upload:
            main._process_put_cmd(
                'url',
                {'path': 'path', 'file_id': 'id'},
                None,
                'some-pod',
                'default',
                MagicMock(),
                mock_logger,
            )
        mock_upload.assert_called_once()
        mock_logger.error.assert_not_called()

    def test_process_put_cmd_ko_timeout(self):
        mock_logger = MagicMock()
        mock_post = MagicMock(return_value=build_agentchannel_mock({}, 200))
        with patch(
            'opentf.operator.main._is_pod_running', MagicMock(return_value=False)
        ), patch('opentf.operator.main._upload_file_to_pod') as mock_upload, patch(
            'requests.post', mock_post
        ):
            main._process_put_cmd(
                'url',
                {'path': 'path', 'file_id': 'id'},
                None,
                'some-pod',
                'default',
                MagicMock(),
                mock_logger,
            )
        mock_upload.assert_not_called()
        mock_post.assert_called_once()
        self.assertIn(
            'Failed to download file "id" to path',
            mock_post.call_args[1]['json']['details']['error'],
        )
        mock_logger.error.assert_called_once_with(
            'An error occurred while downloading file: Timed out, pod still not running.'
        )

    def test_process_put_cmd_ko_notify_ko(self):
        mock_logger = MagicMock()
        mock_post = MagicMock(return_value=build_agentchannel_mock({}, 404))
        with patch(
            'opentf.operator.main._is_pod_running', MagicMock(return_value=False)
        ), patch('opentf.operator.main._upload_file_to_pod') as mock_upload, patch(
            'requests.post', mock_post
        ):
            main._process_put_cmd(
                'url',
                {'path': 'path', 'file_id': 'id'},
                None,
                'some-pod',
                'default',
                MagicMock(),
                mock_logger,
            )
        mock_upload.assert_not_called()
        mock_post.assert_called_once()
        mock_logger.debug.assert_called_once_with(
            'Failed to notify the orchestrator.  Got a %d status code.', 404
        )

    # _process_run_cmd

    def test_process_run_cmd_ok(self):
        cmd = {'filename': 'foo', 'content': 'bar', 'command': 'cmd'}
        with patch(
            'opentf.operator.main._is_pod_running', MagicMock(return_value=True)
        ), patch('opentf.operator.main._upload_file_to_pod') as mock_upload, patch(
            'opentf.operator.main._process_exec_cmd'
        ) as mock_exec:
            main._process_run_cmd('url', cmd, {}, 'pod', 'default', 'rr', 'ss')
        mock_upload.assert_called_once_with('', {}, 'pod', 'default', cmd, 'rr')
        mock_exec.assert_called_once_with('url', cmd, {}, 'pod', 'default', 'rr', 'ss')

    def test_process_run_cmd_ko(self):
        bam = TimeoutError('Bam')
        mock_logger = MagicMock()
        with patch(
            'opentf.operator.main._is_pod_running',
            MagicMock(side_effect=bam),
        ):
            main._process_run_cmd('url', {}, {}, 'pod', 'default', 'zz', mock_logger)
        mock_logger.error.assert_called_once_with('Failed to run command: %s.', bam)

    # _execute_cmd_on_pod

    def test_execute_cmd_on_pod_ok(self):
        mock_api = MagicMock()
        mock_api_instance = mock_api.return_value
        mock_response = MagicMock()
        mock_response.is_open = MagicMock(side_effect=[True, False])
        mock_response.update = MagicMock()
        mock_response.peek_stdout = MagicMock(return_value=True)
        mock_response.peek_stderr = MagicMock(return_value=True)
        mock_response.read_stdout = MagicMock(return_value='some stdout')
        mock_response.read_stderr = MagicMock(return_value='some stderr')
        mock_response.returncode = 0
        with patch(
            'opentf.operator.main.stream.stream', MagicMock(return_value=mock_response)
        ):
            result = main._execute_cmd_on_pod(
                'nice command', 'nice-pod', 'default', mock_api_instance
            )
        self.assertEqual(('some stdout', 'some stderr', 0), result)

    # _is_pod_running

    def test_is_pod_running_true(self):
        mock_api = MagicMock()
        mock_api_instance = mock_api.return_value
        mock_status = MagicMock()
        mock_status.status.phase = 'Running'
        mock_api_instance.read_namespaced_pod_status.return_value = mock_status
        self.assertTrue(main._is_pod_running('some-pod', 'default', mock_api_instance))

    def test_is_pod_running_false(self):
        mock_api = MagicMock()
        mock_api_instance = mock_api.return_value
        mock_status = MagicMock()
        mock_status.status.phase = 'Not running'
        mock_api_instance.read_namespaced_pod_status.return_value = mock_status
        mock_time = MagicMock(side_effect=[1, 2, 101])
        with patch('time.time', mock_time), patch('time.sleep'):
            self.assertFalse(
                main._is_pod_running('some-pod', 'default', mock_api_instance)
            )

    # _upload_file_to_pod

    def test_upload_file_to_pod_ok_put(self):
        mock_api = MagicMock()
        mock_api_instance = mock_api.return_value
        mock_response = MagicMock()
        mock_response.iter_content = MagicMock(return_value=[MagicMock(), MagicMock()])
        mock_response.status_code = 200
        mock_get = MagicMock(return_value=mock_response)
        mock_pod_response = MagicMock()
        mock_stream = MagicMock(return_value=mock_pod_response)
        with patch('requests.get', mock_get), patch(
            'opentf.operator.main.stream.stream', mock_stream
        ):
            main._upload_file_to_pod(
                'url',
                None,
                'some-pod',
                'default',
                {'path': 'foo', 'kind': 'put'},
                mock_api_instance,
            )
        mock_get.assert_called_once()
        self.assertEqual(2, mock_pod_response.write_stdin.call_count)

    def test_upload_file_to_pod_ok_run(self):
        mock_api = MagicMock()
        mock_api_instance = mock_api.return_value
        mock_get = MagicMock()
        mock_pod_response = MagicMock()
        mock_stream = MagicMock(return_value=mock_pod_response)
        with patch('requests.get', mock_get), patch(
            'opentf.operator.main.stream.stream', mock_stream
        ):
            main._upload_file_to_pod(
                'url',
                None,
                'some-pod',
                'default',
                {'filename': 'foo', 'kind': 'run', 'content': 'bar'},
                mock_api_instance,
            )
        mock_get.assert_not_called()
        self.assertEqual(1, mock_pod_response.write_stdin.call_count)

    # _patch_pools

    def test_patch_pools_live_ok(self):
        mock_api = MagicMock()
        mock_api_instance = mock_api.return_value
        with patch(
            'opentf.operator.main.config.load_incluster_config'
        ) as mock_config, patch(
            'opentf.operator.main.client.CustomObjectsApi', mock_api
        ):
            main._patch_pools(
                'some-dead-pool', 'default', {'new': {'living': {'body': 'yay'}}}
            )
        mock_config.assert_called_once()
        mock_api_instance.patch_namespaced_custom_object.assert_called_once()
        self.assertEqual(
            'yay',
            mock_api_instance.patch_namespaced_custom_object.call_args[1]['body'][
                'new'
            ]['living']['body'],
        )

    # _get_live_status

    def test_get_live_status_ko_exception(self):
        boom = Exception('Boom')
        mock_logger = MagicMock()
        mock_api = MagicMock()
        mock_api_instance = mock_api.return_value
        mock_api_instance.get_namespaced_custom_object.side_effect = boom
        with patch('opentf.operator.main.config.load_incluster_config'), patch(
            'opentf.operator.main.client.CustomObjectsApi', mock_api
        ):
            response = main._get_live_status('pool-some', 'default', mock_logger)
        self.assertEqual({}, response)
        mock_logger.error.assert_called_once_with(
            'Error fetching live status: %s.', boom
        )

    # threads

    def test_start_threads_ok(self):
        mock_event = MagicMock()
        mock_thread = MagicMock()
        mock_thread_instance = mock_thread.return_value
        mock_thread_instance.start = MagicMock()
        mock_pools = {}
        with patch('opentf.operator.main.threading.Event', mock_event), patch(
            'opentf.operator.main.threading.Thread', mock_thread
        ), patch('opentf.operator.main.POOLS_THREADS', mock_pools):
            main._start_threads('some-tiny-pool', 'default', {'just': 'to_pass'})
        self.assertEqual(3, mock_event.call_count)
        self.assertEqual(3, mock_thread.call_count)
        self.assertEqual(3, mock_thread_instance.start.call_count)
        self.assertIsNotNone(mock_pools.get('some-tiny-pool.default'))

    def test_stop_threads_ok(self):
        mock_ms = MagicMock()
        mock_hs = MagicMock()
        mock_mt = MagicMock()
        mock_ht = MagicMock()
        mock_as = MagicMock()
        mock_at = MagicMock()
        mock_pools = {
            'some-tiny-pool': {
                'monitor_stop_event': mock_ms,
                'handle_stop_event': mock_hs,
                'active_stop_event': mock_as,
                'monitor_thread': mock_mt,
                'handle_thread': mock_ht,
                'active_thread': mock_at,
            }
        }
        with patch('opentf.operator.main.POOLS_THREADS', mock_pools):
            main._stop_threads('some-tiny-pool')
        self.assertIsNone(mock_pools.get('some-tiny-pool'))
        mock_ms.set.assert_called_once()
        mock_hs.set.assert_called_once()
        mock_as.set.assert_called_once()
        mock_mt.join.assert_called_once()
        mock_ht.join.assert_called_once()
        mock_at.join.assert_called_once()

    def test_stop_threads_threaderror(self):
        with patch('opentf.operator.main.POOLS_THREADS', {}):
            self.assertRaisesRegex(
                main.ThreadError,
                'No thread found for some-tiny-pool.',
                main._stop_threads,
                'some-tiny-pool',
            )

    def test_cleanup_threads_ok(self):
        mock_pools = {'some-tiny-pool': {}, 'some-not-tiny-pool': {}}
        with patch('opentf.operator.main.POOLS_THREADS', mock_pools), patch(
            'opentf.operator.main._stop_threads'
        ) as mock_stop:
            main.cleanup_threads()  # type: ignore
        self.assertEqual(2, mock_stop.call_count)

    def test_cleanup_threads_threaderror(self):
        mock_pools = {'some-tiny-pool': {}, 'some-not-tiny-pool': {}}
        mock_stop = MagicMock(side_effect=[None, main.ThreadError('Failed...')])
        mock_get_logger = MagicMock()
        mock_logger = mock_get_logger.return_value
        with patch('opentf.operator.main.POOLS_THREADS', mock_pools), patch(
            'opentf.operator.main._stop_threads', mock_stop
        ), patch('opentf.operator.main.logging.getLogger', mock_get_logger):
            main.cleanup_threads()  # type: ignore
        self.assertEqual(2, mock_stop.call_count)
        mock_logger.error.assert_called_once_with(
            'Threads cleanup failed: %s.', 'Failed...'
        )

    def test_attempt_agent_removal_ok_no_busy_agents(self):
        agents = ['uuid1', 'uuid2', 'uuid3']
        mock_logger = MagicMock()
        with patch('opentf.operator.main._deregister_agent') as mock_deregister, patch(
            'opentf.operator.main._patch_pools'
        ) as mock_patch, patch('opentf.operator.main.AGENTS_WITH_POD', {}), patch(
            'time.sleep'
        ) as mock_sleep:
            main._attempt_agent_removal(
                agents,
                'some-pool',
                'some-pool-id',
                'default',
                'some-pod',
                {},
                mock_logger,
            )
        mock_sleep.assert_not_called()
        mock_logger.assert_not_called()
        self.assertEqual([], agents)
        self.assertEqual(3, mock_deregister.call_count)
        mock_patch.assert_called_once_with(
            'some-pool', 'default', {'status': {'create_agents': {'agents': []}}}
        )

    def test_attempt_agent_removal_ok_busy_agent(self):
        agents = ['uuid12', 'uuid22', 'uuid33']
        mock_logger = MagicMock()
        mock_agents_pod = MagicMock()
        mock_agents_pod.get.side_effect = [['uuid12'], ['uuid12'], ['uuid12'], []]
        with patch('opentf.operator.main._deregister_agent') as mock_deregister, patch(
            'opentf.operator.main._patch_pools'
        ) as mock_patch, patch(
            'opentf.operator.main.AGENTS_WITH_POD', mock_agents_pod
        ), patch(
            'time.sleep'
        ) as mock_sleep:
            main._attempt_agent_removal(
                agents,
                'some-pool',
                'some-pool-id',
                'default',
                'some-pod',
                {},
                mock_logger,
            )
        mock_sleep.assert_called_once()
        mock_logger.info.assert_called_once_with(
            'Some agents still busy, they will be de-registered on release.'
        )
        self.assertEqual(3, mock_deregister.call_count)
        self.assertEqual(2, mock_patch.call_count)
        self.assertEqual(
            ['uuid12'],
            mock_patch.call_args_list[0][0][2]['status']['create_agents']['agents'],
        )
        self.assertEqual(
            [], mock_patch.call_args_list[1][0][2]['status']['create_agents']['agents']
        )

    # _kill_orphan_agent

    def test_kill_orphan_agent_ok(self):
        mock_logger = MagicMock()
        mock_register = MagicMock(return_value=['agent3'])
        status = {'create_agents': {'agents': ['agent1', 'agent2']}}
        with patch('opentf.operator.main._deregister_agent') as mock_deregister, patch(
            'opentf.operator.main._register_agents', mock_register
        ), patch('opentf.operator.main._patch_pools') as mock_patch:
            main._kill_orphan_agent(
                'pod', 'agent1', {}, 'some-pool', 'default', {}, status, mock_logger
            )
        mock_deregister.assert_called_once_with('agent1', 'pod', {}, mock_logger)
        mock_register.assert_called_once()
        mock_patch.assert_called_once()
        self.assertEqual(
            ['agent2', 'agent3'],
            mock_patch.call_args[0][2]['status']['create_agents']['agents'],
        )

    # _maybe_get_agent_command

    def test_maybe_get_agent_command_204(self):
        mock_logger = MagicMock()
        mock_get = MagicMock(return_value=build_agentchannel_mock({}, 204))
        with patch(
            'opentf.operator.main._get_orchestrator_url', return_value='pod'
        ), patch('opentf.operator.main._make_headers', return_value={}), patch(
            'requests.get', mock_get
        ):
            self.assertIsNone(
                main._maybe_get_agent_command('agent42', {}, 'default', mock_logger)
            )
        mock_get.assert_called_once()

    def test_maybe_get_agent_command_200(self):
        mock_logger = MagicMock()
        mock_get = MagicMock(
            return_value=build_agentchannel_mock({'details': 'some details'}, 200)
        )
        with patch(
            'opentf.operator.main._get_orchestrator_url', return_value='pod'
        ), patch('opentf.operator.main._make_headers', return_value={}), patch(
            'requests.get', mock_get
        ):
            resp = main._maybe_get_agent_command('agent42', {}, 'default', mock_logger)
        mock_get.assert_called_once()
        self.assertEqual('some details', resp)

    def test_maybe_get_agent_command_notfound(self):
        mock_logger = MagicMock()
        mock_get = MagicMock(return_value=build_agentchannel_mock({}, 422))
        with patch(
            'opentf.operator.main._get_orchestrator_url', return_value='pod'
        ), patch('opentf.operator.main._make_headers', return_value={}), patch(
            'requests.get', mock_get
        ):
            self.assertRaisesRegex(
                main.NotFound,
                'Agent agent not found or .details not in response.',
                main._maybe_get_agent_command,
                'agent',
                {},
                'default',
                mock_logger,
            )
        mock_get.assert_called_once()
